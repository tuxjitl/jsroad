let elementId = document.getElementById('parargraph-btn');
elementId.append(createFuncButton('JS created me!', Hello));

function createBtn(text) {
  let btn = document.createElement('button');
  btn.innerHTML += text;
  btn.classList.add('btn', 'btn-outline-success');
  return btn;
}

function createFuncButton(text, func) {
  let button = createBtn(text);
  button.onclick = func;
  return button;
}

function Hello(button) {
  elementId.append(' Hallo Wereld');
}

//##################################################################################################################################################
let text = `
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt totam et dolore odio debitis. Neque ratione mollitia est voluptate repellat eveniet iure, expedita dolor debitis suscipit voluptatem nemo odio delectus?
            Eveniet recusandae facilis nemo temporibus ut, nobis ratione inventore, nesciunt fugiat sit quasi soluta explicabo qui quam consequatur quaerat a. Excepturi aut, sint eaque voluptate velit beatae commodi non voluptatibus.
            Provident quod quos iste quaerat ad adipisci enim aut voluptate molestias omnis necessitatibus odio assumenda in unde distinctio sint, exercitationem minus. Eius rem eveniet facere dolorem obcaecati, officiis quaerat ratione.
            Blanditiis, sequi. Minima at labore quae amet magni debitis voluptatibus similique ex eligendi laborum! Ad repudiandae harum veritatis ducimus quaerat, aspernatur repellendus quasi placeat inventore ea amet possimus magni itaque?
            Molestias fugit mollitia corporis quidem a distinctio saepe, nostrum natus quis sit sed, quia fuga cumque voluptates iste quos ipsam dolore libero voluptatibus quasi in nulla laudantium aspernatur! Expedita, nobis.
            Alias id pariatur exercitationem necessitatibus, suscipit voluptates unde, veritatis velit quas aliquam eos aliquid recusandae tempore vitae eaque. Ipsum at a molestiae laboriosam deserunt? Deleniti eos ex maiores. Cupiditate, fuga.
            Reiciendis et molestias, blanditiis nisi quis harum porro vel tempore temporibus quod reprehenderit, voluptatibus laborum saepe sit non veritatis neque? Minus sunt exercitationem veritatis sapiente tempora temporibus! Quidem, voluptates velit!
            Saepe quaerat tempora molestias sequi, corrupti tempore, iusto aperiam ducimus iste explicabo facilis dolores voluptatibus neque facere ipsa dolor in perspiciatis? Saepe cupiditate minima consequuntur recusandae mollitia voluptatibus, nisi necessitatibus!
            Odit sed eligendi reprehenderit voluptates blanditiis nobis rem consequuntur, similique dolorum quaerat omnis! Quas aspernatur temporibus repellat error nostrum fuga dolorem rerum voluptatum quis, nobis harum corporis quisquam, molestias quia?
            Voluptatum molestias minima beatae cupiditate optio similique laboriosam? Minima, repellat, dolorum maiores consequuntur ipsa porro quas quis error sint accusantium odio dolore dicta autem, atque dolor aliquid assumenda deleniti odit?
`;

let divId = document.getElementById('show-code');
divId.append(createFuncParagraph(text, createParagraph));

function createParagraph(text) {
  let p = document.createElement('p');
  p.innerHTML += text;
  p.classList.add('m-2', 'text-justify', 'text-success');
  return p;
}

function createFuncParagraph(text, func) {
  let paragraph = func(text);
  return paragraph;
}
//##################################################################################################################################################
