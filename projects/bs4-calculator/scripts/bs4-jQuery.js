$(document).ready(function () {
  let result = 0;
  let prevEntry = 0;
  let operation = null;
  let currentEntry = '0';

  updateScreen(result);

  $('#clear').click(function () {
    location.reload();
    console.log('to verify');
  });

  $('.button').on('click', function (evt) {
    let buttonPressed = $(this).html();
    console.log(buttonPressed);

    if (buttonPressed === 'C') {
      result = 0;
      currentEntry = '0';
    } else if (buttonPressed === 'CE') {
      currentEntry = '0';
    } else if (buttonPressed === '+/-') {
      currentEntry *= -1;
    } else if (buttonPressed === '.') {
      currentEntry += '.';
    } else if (isNumber(buttonPressed)) {
      if (currentEntry === '0') {
        currentEntry = buttonPressed;
      } else {
        currentEntry += buttonPressed;
      }
    } else if (isOperator(buttonPressed)) {
      prevEntry = parseFloat(currentEntry);
      operation = buttonPressed;
      currentEntry = '';
    } else if (buttonPressed === '%') {
      currentEntry /= 100;
    } else if (buttonPressed === 'sqrt') {
      currentEntry = Math.sqrt(currentEntry);
    } else if (buttonPressed === '1/x') {
      currentEntry = 1 / currentEntry;
    } else if (buttonPressed === 'PI') {
      currentEntry = Math.PI;
    } else if (buttonPressed === '=') {
      currentEntry = operate(prevEntry, currentEntry, operation);
      operation = null;
    }

    updateScreen(currentEntry);
  });
});

updateScreen = function (displayValue) {
  let displayTextValue = displayValue.toString();
  $('.screen').html(displayTextValue.substring(0, 10));
  $('.text').html(displayTextValue.substring(0, 10));
};

isNumber = function (value) {
  return !isNaN(value);
};

isOperator = function (value) {
  return value === '/' || value === '*' || value === '+' || value === '-';
};

operate = function (prev, cur, op, newCur) {
  prev = parseFloat(prev);
  cur = parseFloat(cur);
  console.log(prev, cur, op);

  if (op === '+') {
    newCur = prev + cur;
    console.log(newCur);
    return newCur;
  }

  if (op === '-') {
    newCur = prev - cur;
    console.log(newCur);
    return newCur;
  }

  if (op === '*') {
    newCur = prev * cur;
    console.log(newCur);
    return newCur;
  }

  if (op === '/') {
    newCur = prev / cur;
    console.log(newCur);
    return newCur;
  }
};
