/*################  general functionality for the site: JQuery  #######################*/
//#region General functionality JQuery
$(document).ready(function () {
  //navbar-toggler
  $('.navbar-toggler').click(function () {
    $('.navbar-toggler').toggleClass('change');
  });
  //video modal play/stop
  $('.modal').each(function () {
    var src = $(this).find('iframe').attr('src');

    $(this).on('click', function () {
      $(this).find('iframe').attr('src', '');
      $(this).find('iframe').attr('src', src);
    });
  });

  // Add smooth scrolling to all links
  $('a').on('click', function (event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== '') {
      // Prevent default anchor click behavior
      event.preventDefault();

      // Store hash
      var hash = this.hash;

      // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
      $('html, body').animate(
        {
          scrollTop: $(hash).offset().top,
        },
        2000,
        function () {
          // Add hash (#) to URL when done scrolling (default click behavior)
          window.location.hash = hash;
        }
      );
    } // End if
  });

  // $('.collapse').collapse();
  
});
//#endregion

/*################  animation header site   ######################*/
//#region animation
function randomValues() {
  anime({
    targets: '#intro-content',
    translateX: function () {
      return anime.random(-700, 700);
    },
    translateY: function () {
      return anime.random(-300, 300);
    },
    easing: 'easeInOutQuad',
    duration: 2200,
    complete: randomValues,
  });
}
randomValues();
//#endregion
