/*##################    Exercises   #########################*/

/*  #region hello world  */
function helloWorld() {
  alert('Hello world');
}
function helloWorldInner() {
  document.getElementById('ex1-03').innerHTML = 'Hello world';
}
function helloWorldConsole() {
  console.log('Hello world');
}
/* #endregion */

/*  #region variables   */
function showMessage(elementId, text) {
  let msg = text;
  document.getElementById(elementId).innerHTML = msg;
}
function showColor(elementId, value) {
  const COLOR_BLUE = '#0000FF';
  // COLOR_BLUE = value;
  document.getElementById(elementId).innerHTML = COLOR_BLUE;
}
function showAdminAlert(yourName) {
  let name = yourName;
  let admin = name;
  alert('The admin is: ' + admin);
}
/* #endregion */

//#region Data types
function showSum(elementId, x, y) {
  if (x !== undefined && y !== undefined) {
    let sum = x + y;
    document.getElementById(elementId).innerHTML = sum;
  } else {
    document.getElementById(elementId).innerHTML =
      'You have to provide 2 numbers.';
  }
}

function showDisplayConcatenation(...args) {
  let argumentList = args;
  let elementId = argumentList[0];
  let textToDisplay = '';

  for (let i = 1; i < argumentList.length; i++) {
    textToDisplay += argumentList[i];
  }

  document.getElementById(elementId).innerHTML = textToDisplay;
}

function showDivision(elementId, x, y) {
  let result = 0;
  result = x / y;

  document.getElementById(elementId).innerHTML = result;
}

function showCheckTypeOf(...args) {
  let elementId = args[0];
  let textToDisplay = '';

  for (let i = 1; i < args.length; i++) {
    textToDisplay = ' => typeof: ' + typeof args[i] + '<br>';
    document.getElementById(elementId).innerHTML +=
      String(args[i]) + textToDisplay;
  }
}

function showShareBirthday(elementId) {
  let nrOfPersons = window.prompt(
    'Please enter a number for persons',
    'Share birthday with...'
  );
  let result = '';
  if (
    nrOfPersons == null ||
    nrOfPersons == '' ||
    nrOfPersons === 'Share birthday with...'
  ) {
    result = 'User cancelled the prompt or did not enter data.';
  } else {
    let chance =
      (1 - Math.pow(364 / 365, (nrOfPersons * (nrOfPersons - 1)) / 2)) * 100;
    chance = Number(chance).toFixed(2);
    result = chance + '%';
  }
  document.getElementById(elementId).innerHTML = result;
}
//#endregion

//#region Operators
function showOperators(elementId) {
  let result = '';
  let answer = 42;
  result += 'Start value: ' + answer + '<br/>';

  answer += 5;
  result += 'Add 5: ' + answer + '<br/>';

  answer /= 7;
  result += 'Divide by 7: ' + answer + '<br/>';

  answer -= 2;
  result += 'Subtract 2: ' + answer + '<br/>';

  answer %= 5;
  result += 'Modulus 5: ' + answer + '<br/>';

  if (answer == 2) {
    result += answer + ' == 2<br/>';
  }
  if (answer == '2') {
    result += answer + " == '2'<br/>";
  }
  if (answer === 2) {
    result += answer + ' === 2<br/>';
  }
  if (answer === 2) {
    result += answer + " === '2'<br/>";
  }
  console.log(result);
  document.getElementById(elementId).innerHTML = result;
}

function showAllowedBeer(elementId) {
  let age = window.prompt('Please enter your age');

  if (!isNaN(age) && age.length > 0) {
    let msg = '';

    if (age < 18) {
      msg = "You're not old enough yet to drink beer.";
    } else {
      msg = "You can drink but you can't drive.";
    }

    document.getElementById(elementId).innerHTML = msg;
  } else {
    document.getElementById(elementId).innerHTML =
      'You have not entered a number';
  }
}

function showQuiz(elementId) {
  let answer = window.prompt('Do you like JavaScript? (Y/N)').toLowerCase();
  console.log(answer);
  let msg = '';

  msg = answer === 'y' ? 'You are sick!!!' : 'You know your limits.';
  document.getElementById(elementId).innerHTML = msg;
}

function showOddsBetween0And100(elementId) {
  let counter = 1;
  let flag = true;
  let result = '';

  while (flag) {
    if (counter > 100) {
      break;
    }
    if (counter % 2 !== 0) {
      if (counter < 99) {
        result += counter + ', ';
      } else {
        result += counter;
      }
    }
    counter++;
  }

  document.getElementById(elementId).innerHTML = result;
}

function showEvenBetween0And200(elementId) {
  let result = '';

  for (let i = 0; i <= 200; i++) {
    if (i % 2 === 0) {
      if (i < 200) {
        result += i + ', ';
      } else {
        result += i;
      }
    }
  }

  document.getElementById(elementId).innerHTML = result;
}

function showFizzBuzz(elementId) {
  let result = '';

  for (let i = 1; i <= 100; i++) {
    if (i % 2 === 0 && i % 3 === 0) {
      if (i < 100) {
        result += 'FizzBuzz, ';
      } else {
        result += 'FizzBuzz';
      }
    }
    if (i % 2 === 0) {
      if (i < 100) {
        result += 'Fizz, ';
      } else {
        result += 'Fizz';
      }
    }
    if (i % 3 === 0) {
      if (i < 100) {
        result += 'Buzz, ';
      } else {
        result += 'Buzz';
      }
    }
  }

  document.getElementById(elementId).innerHTML = result;
}

function showBrowser(elementId) {
  let answer = window.prompt('Which browser do you use?').toLowerCase();

  let msg = '';

  switch (answer) {
    case 'edge':
    case 'chrome':
    case 'firefox':
    case 'safari':
    case 'opera':
      msg = 'We support you!';
      break;
    default:
      msg = 'We hope the page looks ok!';
  }

  document.getElementById(elementId).innerHTML = msg;
}
//#endregion

//#region Functions
function checkBeerAge(age) {
  if (!isNaN(age) && age.length > 0) {
    let canDrink = false;

    if (age >= 18) {
      canDrink = true;
    }
    return canDrink;
  }
}
function showBeer2(elementId) {
  let age = window.prompt('Enter your age');
  let checkingAge = checkBeerAge(age);
  let msg = 'You can not drink.';

  if (checkingAge) {
    msg = 'You can drink, but you can not drive.';
  }

  document.getElementById(elementId).innerHTML = msg;
}
function minVal(a, b) {
  return a < b ? a : b;
}
function getMinValue(elementId) {
  document.getElementById(elementId).innerHTML = minVal(15, -5);
}
//#endregion

//#region Classes

class Person {
  constructor(fName, lName, gender, dob) {
    this.fName = fName;
    this.lName = lName;
    this.gender = gender;
    this.dob = dob;
  }

  calcAge() {
    // let today = new Date().toISOString().slice(0, 10);
    let today = new Date();

    return (today.getFullYear() - this.dob.getFullYear());
  }

  introduce(){
    return "My name is " + this.fName + " " + this.lName + " and I'm " + this.calcAge() + " years old."
  }
}

function showMe(elementId) {
 
  let persons = [
    new Person('Jean-Claude', 'Leurs', 'M', new Date(1969, 1, 30)),
    new Person('Mieke', 'Vanhorensberg', 'V', new Date(1989, 11, 17)),
    new Person('Hans', 'Jansens', 'M', new Date(1990, 8, 13)),
    new Person('Zippy', 'Meelberghs', 'V', new Date(1978, 5, 23))
  ];

  for(let i = 0; persons.length;i++){

    let p = persons[i];
    document.getElementById(elementId).innerHTML += p.introduce() + "<br/>";
  }

  

}
